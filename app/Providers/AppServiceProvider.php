<?php

namespace App\Providers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
use Lib\Services\BuildsService\BuildsService;
use Lib\Services\BuildsService\FileSystemBuildsService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BuildsService::class, function (): BuildsService {
            return new FileSystemBuildsService(Storage::disk('projects'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
