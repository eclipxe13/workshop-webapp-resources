<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;

final class VersionController
{
    public function __invoke(): JsonResponse
    {
        return response()->json([
            'version' => config('app.version', '0.1'),
        ]);
    }
}
