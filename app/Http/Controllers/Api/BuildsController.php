<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lib\Services\BuildsService\BuildsService;
use Lib\Services\BuildsService\ProjectName;
use Symfony\Component\HttpFoundation\StreamedResponse;

final class BuildsController
{
    private BuildsService $service;

    public function __construct(BuildsService $service)
    {
        $this->service = $service;
    }

    public function listRecentBuilds(): JsonResponse
    {
        return response()->json(['data' => $this->service->obtainRecentBuilds()]);
    }

    public function listBuildsByProjectName(Request $request, ProjectName $projectName): JsonResponse
    {
        $recentBuild = $this->service->findRecentBuild($projectName);
        if (! $recentBuild) {
            return response()->json([], 404);
        }

        $requestedPage = max(1, intval($request->query('page') ?? 1));
        $recordsPerPage = max(0, intval($request->query('per_page') ?? 20));
        $offset = (max(1, $requestedPage) - 1) * $recordsPerPage;
        $pagedBuilds = $this->service->obtainPagedBuilds($projectName, $recordsPerPage, $offset);

        return response()->json(array_merge(
            [
                'recent' => $recentBuild,
                'data' => $pagedBuilds->getBuilds(),
            ],
            $pagedBuilds->getPagination()->toPaginationArray()
        ));
    }

    public function badge(ProjectName $projectName): JsonResponse
    {
        $recentBuild = $this->service->findRecentBuild($projectName);
        if (! $recentBuild) {
            return response()->json([], 404);
        }

        $colorMap = [
            'fail/' => 'red',
            'build/' => 'orange',
            'success/updated' => 'green',
            'success/same' => 'blue',
        ];
        $colorKey = sprintf("%s/%s", $recentBuild->getBuildStatus(), $recentBuild->getChangeStatus());

        return response()->json([
            'schemaVersion' => 1,
            'label' => $recentBuild->getName(),
            'message' => date('Y-m-d H:i:s', $recentBuild->getTimestamp()),
            'color' => $colorMap[$colorKey] ?? 'red',
        ]);
    }

    public function showBuild(ProjectName $projectName, int $timestamp): JsonResponse
    {
        $build = $this->service->findBuild($projectName, $timestamp);

        if (! $build) {
            $message = sprintf(
                'The build for project %s timestamp %d was not found',
                $projectName,
                $timestamp
            );
            return response()->json(['message' => $message], 404);
        }

        return response()->json([
            'build' => $build,
            'log' => $this->service->readLog($build->getName(), $build->getTimestamp()),
        ]);
    }

    public function downloadStatus(ProjectName $projectName, int $timestamp): StreamedResponse
    {
        $build = $this->service->findBuild($projectName, $timestamp);

        return response()->streamDownload(
            function () use ($build): void {
                echo $this->service->readStatus($build->getName(), $build->getTimestamp());
            },
            sprintf("%s-%d.json", $projectName, $timestamp),
            ['Content-Type' => 'application/json']
        );
    }

    public function downloadLog(ProjectName $projectName, int $timestamp): StreamedResponse
    {
        $build = $this->service->findBuild($projectName, $timestamp);

        return response()->streamDownload(
            function () use ($build): void {
                echo $this->service->readLog($build->getName(), $build->getTimestamp());
            },
            sprintf("%s-%d.log", $projectName, $timestamp),
            ['Content-Type' => 'application/octet-stream']
        );
    }
}
