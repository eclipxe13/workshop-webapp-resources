<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <base href="{{ url('/') }}/" />

        <title>Laravel</title>

        <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <div id="app"></div>
        <script type="text/javascript">window._apiurl = '{{ url('/api/v1/') }}';</script>
        <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
