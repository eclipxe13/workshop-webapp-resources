import VueRouter from "vue-router";
import Home from "./pages/HomePage.vue";
import ProjectPage from "./pages/ProjectPage.vue";
import BuildPage from "./pages/BuildPage.vue";
import ErrorPage from "./pages/ErrorPage.vue";

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/build/:projectName',
            name: 'project',
            component: ProjectPage,
        },
        {
            path: '/build/:projectName/:timestamp',
            name: 'build',
            component: BuildPage,
        },
        {
            path: '*',
            name: 'error',
            component: ErrorPage,
        },
    ]
})

export default router
