import moment from "moment/moment";

export default class Build {
    constructor(project, date, state, change) {
        this._project = project
        this._date = date
        this._state = state
        this._change = change
    }

    get projectName() {
        return this._project
    }

    get timestamp() {
        return this._date
    }

    get formattedDate() {
        return moment(1000 * this._date).format('YYYY-MM-DD LT')
    }

    get state() {
        if (this.isBuilding) {
            return 'Construyendo...'
        }
        if (this.isError) {
            return 'Falló'
        }
        if (this.isChanged) {
            return 'OK: produjo cambios'
        }
        return 'OK: sin cambios'
    }

    get isBuilding() {
        return 'build' === this._state;
    }

    get isSuccess() {
        return 'success' === this._state;
    }

    get isError() {
        return 'fail' === this._state;
    }

    get isChanged() {
        return this.isSuccess && 'updated' === this._change;
    }

    /**
     * @param {Object} source
     */
    static from(source) {
        return new Build(
            source.project || '(empty)',
            source.date || 0,
            source.state || '',
            source.change || '',
        );
    }
}
