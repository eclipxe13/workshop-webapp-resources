import "./bootstrap";

import Vue from "vue";

import Buefy from 'buefy'
Vue.use(Buefy, {
    defaultIconPack: 'fa',
})

import VueRouter from "vue-router"
Vue.use(VueRouter)

import VueAxios from "vue-axios"
Vue.use(VueAxios, axios)

import App from "./App.vue"
import router from "./router";

// setup Vue.apiurl
Vue.prototype.$apiurl = (suffix) => window._apiurl + '/' + suffix


new Vue({
    el: '#app',
    router,
    render: createElement => createElement(App)
})
