<?php

use App\Http\Controllers\Api\BuildsController;
use App\Http\Controllers\Api\VersionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/version', VersionController::class);
Route::get('/builds/', [BuildsController::class, 'listRecentBuilds'])->name('api');
Route::get('/builds/{projectName}', [BuildsController::class, 'listBuildsByProjectName']);
Route::get('/builds/{projectName}/shields.io', [BuildsController::class, 'badge']);
Route::get('/builds/{projectName}/{timestamp}', [BuildsController::class, 'showBuild']);
Route::get('/builds/{projectName}/{timestamp}/status', [BuildsController::class, 'downloadStatus']);
Route::get('/builds/{projectName}/{timestamp}/log', [BuildsController::class, 'downloadLog']);
Route::get('/{any?}', function () {
    throw new NotFoundHttpException('Invalid route');
})->where('any', '.*');
