# Lista de tareas pendientes

## Tareas actuales

### General

- Revisar las rutas realmente usadas.
- Instrucciones para correr los tests.
- Archivo `README.md`

### API

### Web

- Configurar página de error para que:
    - ¿muestre el mensaje de error?
    - muestre un botón de regresar si es que existe la opción.

## Replantear la aplicacion

Se podrian usar modelos de Laravel, con API completa y seguridad (para poder hacer cambios).

Ahora tendria los siguientes conceptos de *Proyecto* y *Ejecución*.

### Proyecto (project)

Contiene el nombre (tipo slug), descripcion, url, script de contruccion, etc.

Se puede solicitar el proyecto con la última ejecucion.

Se puede solicitar el proyecto con las últimas ejecuciones paginadas.

El proyecto debe tener una programación para ejecutarse

### Ejecución (build)

Depende de *proyecto*.

Tiene el script ejecutado, archivo de registro y estado de la ejecución.

### Tareas manuales y automáticas

Una vez en un proyecto, se podría solicitar correr su script, lo que generará una nueva *ejecución*.

Se deberían utilizar los `Job` para generar una ejecución y tener control sobre su output.

Los trabajos antiguos se podrían purgar diariamente.

Se tendría más información y supervisión de las ejecuciones:

- Se podría saber cuánto tiempo tardó cada paso en ejecutarse así como el tiempo de ejecución total.
- Se podrían levantar eventos de la ejecución, dependiendo de su estado.

#### Eventos de fallo

- Se podrían reportar a los usuarios que están registrados como mantenedores o a Discord, Telegram, etc.

#### Eventos de construcción con cambios

- Se podrían reportar a los usuarios que están registrados con membresía y ejecutar una acción dependiendo del
  tipo de membresía y si está suscrito al proyecto en cuestión.

- A miembros gratuitos se les puede avisar vía correo electrónico.

- A los miembros de paga se les puede ejecutar los *webhooks* que tengan configurados por proyecto.
