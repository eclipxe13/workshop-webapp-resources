# Respuestas API homogéneas

Ponerse de acuerdo en la forma de entregar las respuestas, con 5 casos fundamentales

## Errores

Los errores van **siempre** acompañados de un código de error http.

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "city_id": ["The city id must be an integer.", "The city id field is required."],
        "address": ["The address field is required."]
    }
}
```

## Conjuntos paginados

```json5
{
    "total": 95,        // número de registros totales
    "per_page": 20,     // número de registros por página
    "current_page": 2,  // número de página actual
    "last_page": 5,     // número de páginas totales
    "from": 21,         // número del primer registro 
    "to": 40,           // número del último registro
    "data": [
        {"recordProperty": "value 1"},
        {"recordProperty": "value 2"}
    ],
}
```

## Conjuntos

```json
{
    "data": [
        {"elementProperty": "value 1"},
        {"elementProperty": "value 2"}
    ]
}
```

## Objetos (elementos de un conjunto)

```json
{
    "elementProperty": "value"
}
```
