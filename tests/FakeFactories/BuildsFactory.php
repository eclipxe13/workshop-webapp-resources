<?php

declare(strict_types=1);

namespace Tests\FakeFactories;

use Faker\Generator as Faker;
use Lib\Services\BuildsService\Build;
use Lib\Services\BuildsService\BuildsService;
use Lib\Services\BuildsService\ProjectName;

class BuildsFactory
{
    private Faker $faker;

    private BuildsService $buildsService;

    public function __construct(Faker $faker, BuildsService $buildsService)
    {
        $this->faker = $faker;
        $this->buildsService = $buildsService;
    }

    public function getBuildsService(): BuildsService
    {
        return $this->buildsService;
    }

    public function make(array $data = []): Build
    {
        $buildStatus = $data['state'] ?? $this->faker->randomElement(['build', 'success', 'fail']);
        $changeStatus = $data['change'] ?? (($buildStatus === 'success') ? $this->faker->randomElement(['updated', 'same']) : '');
        return new Build(
            new ProjectName(strval($data['name'] ?? $this->faker->word())),
            $data['date'] ?? $this->faker->dateTimeThisYear()->getTimestamp(),
            $buildStatus,
            $changeStatus,
        );
    }

    public function create(array $data = []): Build
    {
        $build = $this->make($data);
        $this->buildsService->store($build, $data['log'] ?? $this->faker->paragraph());
        return $build;
    }
}
