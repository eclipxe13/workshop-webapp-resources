<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class HomeTest extends DuskTestCase
{
    public function testPageRespond(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertTitle('Laravel');
        });
    }

    public function testContentIsRendered(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Listado de proyectos de construcción continua');
        });
    }
}
