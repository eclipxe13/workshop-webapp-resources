<?php

declare(strict_types=1);

namespace Tests\Api\Feature;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\TestCase;

class ApiRoutesTest extends TestCase
{
    /**
     * @param string $route
     * @testWith [""]
     *           ["/"]
     *           ["/not-found"]
     *           ["/version/not-found"]
     */
    public function testResponseIsConsistentWhenApiRouteIsNotFound(string $route): void
    {
        $response = $this->getJson('/api/v1' . $route);
        $response->assertStatus(404);
        $response->assertJson(['message' => 'Invalid route']);
    }
}
