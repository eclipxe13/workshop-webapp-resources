<?php

declare(strict_types=1);

namespace Tests\Api\Feature;

use Tests\TestCase;

final class VersionTest extends TestCase
{
    public function testCanGetVersion(): void
    {
        $response = $this->get('/api/v1/version');
        $response->assertStatus(200);
        $expectedVersion = config('app.version');
        if (! is_string($expectedVersion)) {
            $this->fail('Cannnot test: Version has not been configured on app.version');
        }
        $response->assertJson(['version' => $expectedVersion]);
    }
}
