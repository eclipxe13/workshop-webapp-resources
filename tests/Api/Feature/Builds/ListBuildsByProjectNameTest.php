<?php

declare(strict_types=1);

namespace Tests\Api\Feature\Builds;

use App\Http\Controllers\Api\BuildsController;
use Lib\Services\BuildsService\PaginationData;
use Tests\WithStorageProjectsTestCase;

final class ListBuildsByProjectNameTest extends WithStorageProjectsTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $factory = $this->newBuildsFactory();
        foreach (range(1, 10) as $i) {
            $factory->create(['name' => 'my-project', 'date' => mktime(6, 0, 0, 1, $i, 2021)]);
        }
    }

    public function testGetBuildsProjectNotFound(): void
    {
        /** @see BuildsController::listBuildsByProjectName() */
        $response = $this->get('/api/v1/builds/non-existent');
        $response->assertStatus(404);
    }

    public function testGetBuilds(): void
    {
        /** @see BuildsController::listBuildsByProjectName() */
        $response = $this->get('/api/v1/builds/my-project');
        $response->assertJsonCount(10, 'data');
        $response->assertJson(PaginationData::newFromOffset(10, 1, 20)->toPaginationArray());
    }

    public function testGetBuildsLimitRecords(): void
    {
        /** @see BuildsController::listBuildsByProjectName() */
        $response = $this->get('/api/v1/builds/my-project?per_page=2');
        $response->assertJsonCount(2, 'data');
        $response->assertJson(PaginationData::newFromOffset(10, 1, 2)->toPaginationArray());
    }

    public function testGetBuildsLimitRecordsLastPage(): void
    {
        /** @see BuildsController::listBuildsByProjectName() */
        $response = $this->get('/api/v1/builds/my-project/?per_page=4&page=3');
        $response->assertJson(['data' => [0 => ['date' => mktime(6, 0, 0, 1, 2, 2021)]]]);
        $response->assertJson(['data' => [1 => ['date' => mktime(6, 0, 0, 1, 1, 2021)]]]);
        $response->assertJsonCount(2, 'data');
        $response->assertJson(PaginationData::newFromOffset(10, 9, 4)->toPaginationArray());
    }

    public function testGetBuildsLimitRecordsFirstPage(): void
    {
        /** @see BuildsController::listBuildsByProjectName() */
        $response = $this->get('/api/v1/builds/my-project/?per_page=2&page=1');
        $response->assertJson(['data' => [0 => ['date' => mktime(6, 0, 0, 1, 10, 2021)]]]);
        $response->assertJson(['data' => [1 => ['date' => mktime(6, 0, 0, 1, 9, 2021)]]]);
        $response->assertJsonCount(2, 'data');
        $response->assertJson(PaginationData::newFromOffset(10, 1, 2)->toPaginationArray());
    }
}
