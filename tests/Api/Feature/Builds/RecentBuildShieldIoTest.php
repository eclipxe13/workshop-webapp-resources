<?php

declare(strict_types=1);

namespace Tests\Api\Feature\Builds;

use Tests\WithStorageProjectsTestCase;

class RecentBuildShieldIoTest extends WithStorageProjectsTestCase
{
    /**
     * @param string $color
     * @param string $state
     * @param string $change
     * @dataProvider providerShieldProperties
     */
    public function testShieldProperties(string $color, string $state, string $change = ''): void
    {
        $factory = $this->newBuildsFactory();
        $build = $factory->create([
            'state' => $state,
            'change' => $change,
        ]);
        $response = $this->get(sprintf('/api/v1/builds/%s/shields.io', $build->getName()));
        $expected = [
            'schemaVersion' => 1,
            'label' => $build->getName(),
            'message' => date('Y-m-d H:i:s', $build->getTimestamp()),
            'color' => $color,
        ];
        $response->assertJson($expected);
    }

    public function providerShieldProperties(): array
    {
        return [
            'success/same' => ['blue', 'success', 'same'],
            'success/change' => ['green', 'success', 'updated'],
            'building' => ['orange', 'build'],
            'fail' => ['red', 'fail'],
        ];
    }

}
