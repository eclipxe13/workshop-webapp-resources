<?php

declare(strict_types=1);

namespace Tests\Api\Feature\Builds;

use App\Http\Controllers\Api\BuildsController;
use Tests\WithStorageProjectsTestCase;

final class ListRecentBuildsTest extends WithStorageProjectsTestCase
{
    public function testGetRecentBuilds(): void
    {
        /** @see BuildsController::listRecentBuilds */
        $factory = $this->newBuildsFactory();
        $foo = $factory->create(['name' => 'foo']);
        $bar = $factory->create(['name' => 'bar']);

        $response = $this->get('/api/v1/builds/');

        $response->assertOk();
        $response->assertJson(['data' => [$bar->jsonSerialize(), $foo->jsonSerialize()]]);
    }
}
