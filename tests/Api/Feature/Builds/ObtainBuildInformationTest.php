<?php

declare(strict_types=1);

namespace Tests\Api\Feature\Builds;

use App\Http\Controllers\Api\BuildsController;
use Lib\Services\BuildsService\Build;
use Tests\WithStorageProjectsTestCase;

final class ObtainBuildInformationTest extends WithStorageProjectsTestCase
{
    private Build $build;

    protected function setUp(): void
    {
        parent::setUp();

        $factory = $this->newBuildsFactory();
        $build = $factory->create(['log' => 'Lorem ipsum']);
        $factory->create(['name' => $build->getName(), 'date' => $build->getTimestamp() - 1]);
        $factory->create(['name' => $build->getName(), 'date' => $build->getTimestamp() + 1]);
        $this->build = $build;
    }

    public function testGetBuildByNameAndTimestamp(): void
    {
        /** @see BuildsController::showBuild() */
        $build = $this->build;
        $response = $this->get(sprintf('/api/v1/builds/%s/%d', $build->getName(), $build->getTimestamp()));

        $response->assertStatus(200);
        $response->assertJson(['build' => $build->jsonSerialize()]);
        $this->assertNotEmpty($response->json('log'));
    }

    public function testGetNonExistentBuild(): void
    {
        /** @see BuildsController::showBuild() */
        $build = $this->build;
        $response = $this->get(sprintf('/api/v1/builds/%s/%d', $build->getName(), $build->getTimestamp() + 2));

        $response->assertStatus(404);
        $response->assertJsonStructure(['message']);
    }

    public function testDownloadStatusFileByNameAndTimestamp(): void
    {
        /** @see BuildsController::downloadStatus() */
        $build = $this->build;
        $response = $this->get(sprintf('/api/v1/builds/%s/%d/status', $build->getName(), $build->getTimestamp()));

        $response->assertOk();
        $response->assertHeader('Content-Type', 'application/json');
        $expectedFilename = sprintf('%s-%d.json', $build->getName(), $build->getTimestamp());
        $response->assertHeader('Content-Disposition', "attachment; filename=$expectedFilename");
        $this->assertJsonStringEqualsJsonString(json_encode($build), $response->streamedContent());
    }

    public function testDownloadBuildLogFileByNameAndTimestamp(): void
    {
        /** @see BuildsController::downloadLog() */
        $build = $this->build;
        $expectedFilename = sprintf('%s-%d.log', $build->getName(), $build->getTimestamp());
        $response = $this->get(sprintf('/api/v1/builds/%s/%d/log', $build->getName(), $build->getTimestamp()));

        $response->assertOk();
        $response->assertHeader('Content-Type', 'application/octet-stream');
        $response->assertHeader('Content-Disposition', "attachment; filename=$expectedFilename");
        $this->assertEquals('Lorem ipsum', $response->streamedContent());
    }
}
