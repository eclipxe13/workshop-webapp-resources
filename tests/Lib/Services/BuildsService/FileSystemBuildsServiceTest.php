<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Tests\Lib\Services\BuildsService;

use Illuminate\Support\Facades\Storage;
use Lib\Services\BuildsService\BuildsService;
use Lib\Services\BuildsService\FileSystemBuildsService;
use Tests\FakeFactories\BuildsFactory;
use Tests\TestCase;

final class FileSystemBuildsServiceTest extends TestCase
{
    public function testCanCreateBuildsService(): void
    {
        $service = $this->app->make(BuildsService::class);
        $this->assertInstanceOf(BuildsService::class, $service);
        $this->assertInstanceOf(FileSystemBuildsService::class, $service);
    }

    public function testCanStoreBuild(): void
    {
        Storage::fake('projects');
        /** @var BuildsFactory $factory */
        $factory = $this->app->make(BuildsFactory::class);
        /** @var FileSystemBuildsService $service */
        $service = $factory->getBuildsService();

        $build = $factory->make();
        $service->store($build, 'log file contents');
        $this->assertEquals($build, $service->findBuild($build->getName(), $build->getTimestamp()));
    }
}
