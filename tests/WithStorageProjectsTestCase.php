<?php

declare(strict_types=1);

namespace Tests;

use Illuminate\Support\Facades\Storage;
use Tests\FakeFactories\BuildsFactory;

abstract class WithStorageProjectsTestCase extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        Storage::fake('projects');
    }

    public function newBuildsFactory(): BuildsFactory
    {
        /** @var BuildsFactory $factory */
        $factory = $this->app->make(BuildsFactory::class);
        return $factory;
    }
}
