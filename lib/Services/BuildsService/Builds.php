<?php

declare(strict_types=1);

namespace Lib\Services\BuildsService;

final class Builds implements \JsonSerializable
{
    private array $builds;

    public function __construct(Build ...$builds)
    {
        $this->builds = $builds;
    }

    public function jsonSerialize(): array
    {
        return $this->builds;
    }
}
