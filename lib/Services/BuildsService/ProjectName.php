<?php

declare(strict_types=1);

namespace Lib\Services\BuildsService;

use JsonSerializable;

class ProjectName implements JsonSerializable
{
    private string $value;

    public function __construct(string $projectName)
    {
        $this->value = $projectName;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function jsonSerialize(): string
    {
        return $this->value;
    }
}
