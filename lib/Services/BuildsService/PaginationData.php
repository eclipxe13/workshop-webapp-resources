<?php

declare(strict_types=1);

namespace Lib\Services\BuildsService;

class PaginationData
{
    private int $totalRecords;
    private int $recordsPerPage;
    private int $currentPage;
    private int $totalPages;
    private int $firstRecord;
    private int $lastRecord;

    public function __construct(
        int $totalRecords,
        int $recordsPerPage,
        int $currentPage,
        int $totalPages,
        int $firstRecord,
        int $lastRecord
    ) {
        $this->totalRecords = $totalRecords;
        $this->recordsPerPage = $recordsPerPage;
        $this->currentPage = $currentPage;
        $this->totalPages = $totalPages;
        $this->firstRecord = $firstRecord;
        $this->lastRecord = $lastRecord;
    }

    public static function newFromOffset(int $total, int $current, int $pageSize): self
    {
        $totalPages = (int) ceil($total / $pageSize);
        $currentPage = (int) ceil($current / $pageSize);
        $lastRecord = min($current + $pageSize, $total);

        return new self(
            $total,
            $pageSize,
            $currentPage,
            $totalPages,
            $current,
            $lastRecord
        );
    }

    /** @return array<string, int> */
    public function toPaginationArray(): array
    {
        return [
            'total' => $this->totalRecords,
            'per_page' => $this->recordsPerPage,
            'current_page' => $this->currentPage,
            'last_page' => $this->totalPages,
            'from' => $this->firstRecord,
            'to' => $this->lastRecord,
        ];
    }
}
