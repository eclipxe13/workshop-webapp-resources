<?php

declare(strict_types=1);

namespace Lib\Services\BuildsService;

use JsonSerializable;

final class Build implements JsonSerializable
{
    private ProjectName $name;

    private int $timestamp;

    private string $buildStatus;

    private string $changeStatus;

    public function __construct(ProjectName $name, int $timestamp, string $buildStatus, string $changeStatus)
    {
        $this->name = $name;
        $this->timestamp = $timestamp;
        $this->buildStatus = $buildStatus;
        $this->changeStatus = $changeStatus;
    }

    public static function newFromJson(string $json): self
    {
        $data = json_decode($json);
        return new self(
            new ProjectName(strval($data->project ?? '')),
            intval($data->date ?? 0),
            strval($data->state ?? ''),
            strval($data->change ?? ''),
        );
    }

    public function getName(): ProjectName
    {
        return $this->name;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getBuildStatus(): string
    {
        return $this->buildStatus;
    }

    public function getChangeStatus(): string
    {
        return $this->changeStatus;
    }

    public function jsonSerialize(): array
    {
        return [
            'project' => $this->getName(),
            'date' => $this->getTimestamp(),
            'state' => $this->getBuildStatus(),
            'change' => $this->getChangeStatus(),
        ];
    }
}
