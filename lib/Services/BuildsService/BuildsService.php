<?php

declare(strict_types=1);

namespace Lib\Services\BuildsService;

interface BuildsService
{
    public function obtainRecentBuilds(): Builds;

    public function findRecentBuild(ProjectName $projectName): ?Build;

    public function obtainPagedBuilds(ProjectName $projectName, int $count, int $offset): PagedBuilds;

    public function findBuild(ProjectName $projectName, int $timestamp): ?Build;

    public function readStatus(ProjectName $projectName, int $timestamp): string;

    public function readLog(ProjectName $projectName, int $timestamp): string;
}
