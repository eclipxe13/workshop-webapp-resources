<?php

declare(strict_types=1);

namespace Lib\Services\BuildsService;

class PagedBuilds
{
    private Builds $builds;

    private PaginationData $pagination;

    public function __construct(Builds $builds, PaginationData $pagination)
    {
        $this->builds = $builds;
        $this->pagination = $pagination;
    }

    public function getBuilds(): Builds
    {
        return $this->builds;
    }

    public function getPagination(): PaginationData
    {
        return $this->pagination;
    }
}
