<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Lib\Services\BuildsService;

use Illuminate\Contracts\Filesystem\Filesystem;

final class FileSystemBuildsService implements BuildsService
{
    private Filesystem $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function obtainRecentBuilds(): Builds
    {
        $builds = [];
        foreach ($this->filesystem->directories() as $directory) {
            $statePath = $this->recentStatusPath($directory);
            if ($this->filesystem->exists($statePath)) {
                $builds[] = Build::newFromJson($this->filesystem->get($statePath));
            }
        }
        return new Builds(...$builds);
    }

    public function findRecentBuild(ProjectName $projectName): ?Build
    {
        $stateFile = $this->recentStatusPath($projectName);
        if (! $this->filesystem->exists($stateFile)) {
            return null;
        }
        return Build::newFromJson($this->filesystem->get($stateFile));
    }

    public function obtainPagedBuilds(ProjectName $projectName, int $count, int $offset): PagedBuilds
    {
        $logs = $this->logsPath($projectName);
        $stateFiles = [];
        $current = 0;
        $maxOffset = $offset + $count - 1;
        $files = $this->filesystem->files($logs);
        rsort($files);
        foreach ($files as $file) {
            $stateFile = basename($file);
            if (! preg_match('/^\d{8}-\d{6}\.state$/', $stateFile)) {
                continue;
            }
            $logFile = substr($stateFile, 0, -6) . '.log';
            if (! $this->filesystem->exists($logs . '/' . $logFile)) {
                continue;
            }
            if ($current >= $offset && ($count === 0 || $current <= $maxOffset)) {
                $stateFiles[] = $file;
            }
            $current = $current + 1;
        }
        $total = $current;
        $builds = array_map(
            fn(string $stateFile): Build => Build::newFromJson($this->filesystem->get($stateFile)),
            $stateFiles
        );
        return new PagedBuilds(new Builds(...$builds), PaginationData::newFromOffset($total, $offset + 1, $count));
    }

    public function findBuild(ProjectName $projectName, int $timestamp): ?Build
    {
        $stateFile = $this->buildStatusPath($projectName, $timestamp);
        if (! $this->filesystem->exists($stateFile)) {
            return null;
        }
        return Build::newFromJson($this->filesystem->get($stateFile));
    }

    public function readLog(ProjectName $projectName, int $timestamp): string
    {
        $stateFile = $this->buildLogPath($projectName, $timestamp);
        if (! $this->filesystem->exists($stateFile)) {
            return '';
        }
        return $this->filesystem->get($stateFile);
    }

    public function readStatus(ProjectName $projectName, int $timestamp): string
    {
        $stateFile = $this->buildStatusPath($projectName, $timestamp);
        if (! $this->filesystem->exists($stateFile)) {
            return '';
        }
        return $this->filesystem->get($stateFile);
    }

    public function store(Build $build, string $logFileContents): void
    {
        // store state
        $stateFile = $this->buildStatusPath($build->getName(), $build->getTimestamp());
        $this->filesystem->put($stateFile, json_encode($build));
        // store log
        $logFile = $this->buildLogPath($build->getName(), $build->getTimestamp());
        $this->filesystem->put($logFile, $logFileContents);

        // reset recent state
        $recentBuild = $this->findRecentBuild($build->getName());
        if (null === $recentBuild || $recentBuild->getTimestamp() < $build->getTimestamp()) {
            // set up most recent state
            $stateFile = $this->recentStatusPath($build->getName());
            $this->filesystem->put($stateFile, json_encode($build));
        }
    }

    private function recentStatusPath($projectName): string
    {
        return sprintf('%s/state.json', $projectName);
    }

    private function logsPath($projectName): string
    {
        return sprintf('%s/logs', $projectName);
    }

    private function buildStatusPath($projectName, int $timestamp): string
    {
        return sprintf('%s/%s', $this->logsPath($projectName), $this->timestampToFilename($timestamp, '.state'));
    }

    private function buildLogPath($projectName, int $timestamp): string
    {
        return sprintf('%s/%s', $this->logsPath($projectName), $this->timestampToFilename($timestamp, '.log'));
    }

    private function timestampToFilename(int $timestamp, string $suffix): string
    {
        return date('Ymd-His', $timestamp) . $suffix;
    }
}
